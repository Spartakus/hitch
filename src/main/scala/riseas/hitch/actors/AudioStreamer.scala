package riseas.hitch.actors

import scala.concurrent.ExecutionContext.Implicits.global
import akka.actor.{Actor, ActorLogging, ActorRef, PoisonPill, Terminated}
import akka.cluster.Cluster
import akka.cluster.ClusterEvent.MemberUp
import akka.event.LoggingAdapter
import riseas.hitch._

import scala.util.{Failure, Random, Success}
import scala.concurrent.Future
import sys.process._

class AudioStreamer  extends Actor with ActorLogging {


  val cluster = Cluster(context.system)

  override def preStart(): Unit = cluster.subscribe(self,  classOf[MemberUp])
  override def postStop(): Unit = cluster.unsubscribe(self)



  var listeners: IndexedSeq[ActorRef] = IndexedSeq.empty[ActorRef]

  /** audiostream is running */
  def listener(audioStreamURI: String): Receive = {

    case SubscribeToAudioStream =>
      context watch sender()
      listeners :+ sender()
      sender() ! AudioSubscription( audioStreamURI )

    case Terminated(a) => // Filter out dead listeners
      listeners = listeners.filterNot( _ == a)
      if (listeners.isEmpty)
        self ! PoisonPill

    case _ => // Nothing

  }

  /** Audio Stream is to be set up */
  def startup: Receive = {
    case request: CreateAudioStream =>
      log.info(s"Starting AudioStream for ${request.videoStream}")
      val originalSender = sender
      val audioStreamURL = AudioStreamer.createAudioStreamFromHLS(log, request.videoStream)

      audioStreamURL onComplete {
        case Failure(e) =>
          log.error(e, "Creation failed!!!")
        case Success(link) =>
          log.info("We got a Audiostream")
          originalSender ! AudioStreamCreated(self, request)
          context.become(listener(link))


        case _ => // Error Case - Should not happen (thus, will happen 100%..)
      }
  }

  def receive: PartialFunction[Any, Unit] = startup

}

object AudioStreamer {


  val path: java.net.URL = getClass.getResource("/ffmpeg/bin/ffmpeg.exe")

  def startStream(log: LoggingAdapter, streamURL:String): Future[String] = {
    Future {

      val outputString = Random.alphanumeric.take(10).mkString("")
      val playlistPath = outputString+".m3u8"
      val segmentsPath = outputString + "out%03d.ts"

      val commandString: List[String] = List(
        path.getPath,
        "-i", streamURL, "-vn",
        "-codec:a aac",
        "-f ssegment",
        "-segment_list", playlistPath,
        "-segment_list_flags",
        "+live",
        "-segment_time 10",
        segmentsPath
      )

      log.info(s"Executing the following: ${commandString.mkString(" ")}")

      val output = commandString.mkString(" ").lineStream_!.toList
      playlistPath
    }
  }

  def createAudioStreamFromHLS(log: LoggingAdapter, streamURL: String): Future[String] = {
    startStream(log, streamURL)
  }
}
