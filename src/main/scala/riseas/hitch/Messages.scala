package riseas.hitch

import akka.actor.ActorRef

import scala.concurrent.Future


final case class LinkRequested(url: String)

final case class StreamlinkRecieved(uri: String, job: LinkRequested)
final case class JobFailed(reason: String, job: LinkRequested)

final case class CreateAudioStream(videoStream: String)
final case class FailedCreatingAudioStream(reason: String, job: CreateAudioStream)

final case class AudioStreamCreated(responsibleActor: ActorRef, job: CreateAudioStream)
final case class CreationAcknowledged(job: CreateAudioStream)

final case class Echo(text: String)
final case class EchoResponse(response: String, source: Echo)

case object SubscribeToAudioStream
final case class AudioSubscription( audioStreamURL: String )

case object BackendRegistration
case object StreamlinkRegistration
case object AudioMasterRegistration