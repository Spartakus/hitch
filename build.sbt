organization := "com.gitlab.riseas"
name := "hitch"
version := "0.1"

scalaVersion := "2.12.3"
// Compiler settings. Use scalac -X for other options and their description.
// See Here for more info http://www.scala-lang.org/files/archive/nightly/docs/manual/html/scalac.html
scalacOptions ++= List("-feature","-deprecation", "-unchecked", "-Xlint")
resolvers += "Scalaz Bintray Repo" at "http://dl.bintray.com/scalaz/releases"
unmanagedJars in Compile += Attributed.blank(file(System.getenv("JAVA_HOME") + "/lib/ext/jfxrt.jar"))

addCompilerPlugin("org.scalamacros" %% "paradise" % "2.1.0" cross CrossVersion.full)
libraryDependencies ++= Seq(
  "org.scalatest"     %% "scalatest"             % "3.0.1"   % "test",
  "org.scalacheck"    %% "scalacheck"            % "1.13.4"  % "test",
  "com.typesafe.akka" %% "akka-cluster"          % "2.5.4",
  "com.typesafe.akka" %% "akka-cluster-tools"    % "2.5.4",
  "com.typesafe.akka" %% "akka-cluster-metrics"  % "2.5.4",
  "io.humble"         % "humble-video-all"       % "0.2.1",
  "com.twitter"       %% "storehaus-cache"       % "0.15.0",
  "com.typesafe.play" %% "play-json"             % "2.6.6",
  "org.scalafx"       %% "scalafx"               % "8.0.144-R12",
  "org.scalafx"       %% "scalafxml-core-sfx8"   % "0.4"


)




lazy val main = taskKey[Unit]("Run Main")
main := (runMain in Compile).toTask(" riseas.hitch.App").value

lazy val runAll = taskKey[Unit]("Run all Nodes")
runAll := (runMain in Compile).toTask(" riseas.hitch.actors.Frontend").value


lazy val runFrontend = taskKey[Unit]("Run all Nodes")
runFrontend := (runMain in Compile).toTask(" riseas.hitch.actors.Frontend").value

lazy val runStreamLink = taskKey[Unit]("Run all Nodes")
runStreamLink := (runMain in Compile).toTask(" riseas.hitch.actors.StreamLink").value

/** These Things are to run **/

lazy val runFrontendSeed = taskKey[Unit]("Run all FrontEnd Seed")
runFrontendSeed := (runMain in Compile).toTask(" riseas.hitch.actors.Frontend 2551").value
lazy val runStreamLinkSeed = taskKey[Unit]("Run all StreamLink Seed")
runStreamLinkSeed := (runMain in Compile).toTask(" riseas.hitch.actors.StreamLink 2552").value
lazy val runAudiomaster = taskKey[Unit]("Run Audio Master")
runAudiomaster := (runMain in Compile).toTask(" riseas.hitch.actors.AudioMaster").value
//{
//  (runMain in Compile).toTask(" riseas.hitch.actors.Frontend 2551").value
//  (runMain in Compile).toTask(" riseas.hitch.actors.Frontend 2552").value
//  (runMain in Compile).toTask(" riseas.hitch.actors.StreamLink").value
//}