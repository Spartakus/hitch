package riseas.hitch

import java.io.IOException

import akka.dispatch.RequiresMessageQueue
import akka.event.LoggerMessageQueueSemantics
import akka.event.Logging._
//import javafx.fxml.FXMLLoader
import javafx.scene.Parent

import akka.actor.{Actor, ActorLogging, ActorPath, ActorRef, ActorSystem, Props}
import akka.cluster.client.{ClusterClient, ClusterClientSettings}
import com.typesafe.config.ConfigFactory

import scala.concurrent.duration._
import riseas.hitch.HitchBoot.{getClass, system}
import riseas.hitch.actors.{AudioMaster, Frontend, StreamLink}

import scalafx.application.JFXApp
import scala.concurrent.Future
import scalafx.Includes._
import scalafx.application.JFXApp
import scalafx.application.JFXApp.PrimaryStage
import scalafx.scene.Scene
import scalafxml.core.{FXMLLoader, FXMLView, NoDependencyResolver}
import scalafx.Includes._
import scalafx.scene.layout.Pane

class HitchBoot(gui: GUIState) extends Actor {

  val initialContacts = Set(
    ActorPath.fromString("akka.tcp://HitchSystem@127.0.0.1:2551/system/receptionist"))



  def clusterclient(cc: ActorRef): Receive = {
    case m: Echo =>
      cc ! ClusterClient.Send("/user/Frontend",  m, localAffinity = true)

    case EchoResponse(response: String, _) =>
      gui.addConsoleLine(s"Add Answere: $response")

    case _ =>  gui.addConsoleLine("I recieved something - client established")
  }

  override def receive: Receive = {
    case "START" =>
      val c = system.actorOf(ClusterClient.props(
        ClusterClientSettings(system).withInitialContacts(initialContacts)), "client")
        context.become(clusterclient(c))
    case _ =>  gui.addConsoleLine("I recieved something - no client yet")
  }
}
object HitchBoot {
  val config = ConfigFactory.parseString(""" """).
    withFallback(ConfigFactory.load())
  val system = ActorSystem("GUISystem", config)
  def start(gui: GUIState): ActorRef = {
    println("Booting Actors")

    val mapArgs: String => Array[String] = {
      case "streamlinkNoSeed" => Array.empty
      case "streamLinkSeed" => Array("2552")
      case "frontendSeed" => Array[String]("2551")
      case "frontendNoSeed" => Array.empty
      case "audioMaster" => Array.empty
      case _ => Array.empty
    }

    StreamLink.main(mapArgs("streamLinkSeed"))
    Frontend.main(mapArgs("frontendSeed"))
    AudioMaster.main(mapArgs("audioMaster"))

    val myselfActor = system.actorOf(Props(new HitchBoot(gui)), name = "GUI")

    myselfActor ! "START"

    myselfActor
  }
}

object App extends JFXApp {
  val resource = getClass.getResource("/gui/gui.fxml")
  if (resource == null) {
    println("There is no resource man.")
    throw new IOException("Not found")
  }

 //  val root:Parent = FXMLLoader.load(resource)
 //  val root = FXMLView(resource, NoDependencyResolver)/*
  val loader = new FXMLLoader(resource, NoDependencyResolver  )
  loader.load()

  val root = loader.getRoot[javafx.scene.Parent]

  stage = new PrimaryStage {
    title = "FXML GridPane Demo"
    scene = new Scene(root)
  }

  val controller: GUIState = loader.getController[GUIState]
  val actor: ActorRef = HitchBoot.start(controller)

  val Pattern = "[Ee][Cc][Hh][Oo] (.*)".r
  val callback: String => Unit = {
      case Pattern(arg) => actor ! Echo(arg)
      case "ECHO" => actor ! "DO ECHO"
      case "HELP" => controller.addLogLine("He typed Help!")
      case _ => controller.addLogLine("WTF did you type?")
  }

  controller.addButtonEvent(callback)
}
