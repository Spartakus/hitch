package riseas.hitch.actors

import akka.cluster.{Cluster, Member, MemberStatus}
import akka.cluster.ClusterEvent._
import akka.actor.{Actor, ActorLogging, ActorSystem, Props, RootActorPath}
import com.typesafe.config.ConfigFactory
import riseas.hitch.{JobFailed, LinkRequested, StreamlinkRecieved, StreamlinkRegistration}

import sys.process._
import scala.language.postfixOps
import scala.concurrent.Future
import com.twitter.conversions.time._
import com.twitter.storehaus.cache._
import com.twitter.util.Duration
import play.api.libs.json.Json


import scala.concurrent.ExecutionContext.Implicits.global

sealed trait CacheEntry
case class Hit(url:String) extends CacheEntry
case class Miss(reason:String) extends CacheEntry

class StreamLink extends Actor with ActorLogging {

  val cluster = Cluster(context.system)

  def answere(results: TTLCache[String, CacheEntry]): Receive = {

    case MemberUp(member) => register(member)
    case state: CurrentClusterState => state.members.filter(_.status == MemberStatus.Up) foreach register
    case _: MemberEvent => // ignore
    case job: LinkRequested =>

      val maybeAnswere = results.getNonExpired(job.url)

      maybeAnswere match {
        case Some(answere) =>
          answere match {
            case Hit(url) =>
              sender() ! StreamlinkRecieved( url, job)
            case Miss(reason) =>
              sender() ! JobFailed( reason, job)
          }
        case None =>
          //log.info(s"Recieve Link for ${job.url}")
          val result = StreamLink.getStreamMeta(job.url)

          result.map { metaData =>

              val extractedStreamURL = StreamLink.extractStreamURL(metaData)

              extractedStreamURL match {
                case Some(streamURL) =>

                  log.info(s"[FOUND] Adding to context ${job.url} with MATCH $streamURL")
                  sender() ! StreamlinkRecieved( streamURL, job)

                  log.info(s"Adding to context ${job.url} with value $streamURL")

                  val (set, newMap) = results.putClocked(job.url -> Hit(streamURL))
                  context.become( answere( newMap ) )

                case None =>
                  val reason = "No Streamlink found there"
                  log.info(s"[MISS] Adding to context ${job.url} with MISS $reason")


                  sender() !  JobFailed(reason, job)

                  val now = System.currentTimeMillis
                  val (set, newMap) = results.putClocked(job.url -> Miss(reason))
                  context.become( answere( newMap ) )
              }

          }
      }
  }

  // subscribe to cluster changes, re-subscribe when restart
  override def preStart(): Unit = {
    //log.info(s"Found Streamlink at: ${path.getPath}")
    cluster.subscribe(self,  classOf[MemberUp])
  }

  override def postStop(): Unit = cluster.unsubscribe(self)

  def receive = answere(TTLCache[String, CacheEntry](30.seconds))



  def register(member: Member): Unit = {
    if (member.hasRole("Frontend")) {
      log.info(s"Frontend Node recieved $sender")
      context.actorSelection(RootActorPath(member.address) / "user" / "Frontend") !  StreamlinkRegistration
    } else
      log.info(s"Another Node was started: $sender")
  }

}

object StreamLink    {
  def main(args: Array[String]): Unit = {
    // Override the configuration of the port when specified as program argument
    val port = if (args.isEmpty) "0" else args(0)
    val config = ConfigFactory.parseString(s"akka.remote.netty.tcp.port=$port").
      withFallback(ConfigFactory.parseString("akka.cluster.roles = [StreamLink]")).
      withFallback(ConfigFactory.load())

    val system = ActorSystem("HitchSystem", config)
    val streamlink = system.actorOf(Props[StreamLink], name = "StreamLink")
  }


  val path: java.net.URL = getClass.getResource("/streamlink/Streamlink.exe")

  def extractStreamURL(jsonString: String): Option[String] = {
    val json = Json.parse(jsonString)
    (json \ "url").asOpt[String]
  }

  def getStreamMeta(url:String): Future[String] = {
    Future {
      val executionString = s"${path.getPath} -j $url best"
      val output = executionString.lineStream_!.toList

      output.slice(0, output.size - 1).tail.mkString("")
    }
  }

}
