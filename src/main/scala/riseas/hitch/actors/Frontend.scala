package riseas.hitch.actors


import java.util.concurrent.atomic.AtomicInteger

import language.postfixOps
import scala.concurrent.duration._
import akka.cluster.ClusterEvent._
import akka.actor.{Actor, ActorLogging, ActorRef, ActorSystem, Props, RootActorPath, Terminated}
import akka.cluster.Cluster
import akka.cluster
import akka.cluster.client.ClusterClientReceptionist
import akka.pattern.ask
import akka.util.Timeout
import com.typesafe.config.ConfigFactory
import riseas.hitch._

import scala.concurrent.Future



class Frontend extends Actor with ActorLogging {


  val cluster = Cluster(context.system)
  val audiomaster = context.actorSelection(RootActorPath(cluster.selfAddress) / "user" / "Audiomaster")

  var streamLinkRefs = IndexedSeq.empty[ActorRef]
  var audioMasterRef: Option[ActorRef] = None


  override def preStart(): Unit = cluster.subscribe(self,  classOf[MemberUp])
  override def postStop(): Unit = cluster.unsubscribe(self)
  //override def preStart(): Unit = log.info(s"Starting Frontend Node! ${self.path}")

  import scala.concurrent.ExecutionContext.Implicits.global

  val r = new scala.util.Random

  def start = 0
  def end = streamLinkRefs.size

  def frontEndLogic(maybeAudioMaster:Option[ActorRef]): Receive = {

    case x: Echo =>
      sender() ! EchoResponse(x.text, x)

    case AudioSubscription( audioStreamURI ) =>
      log.info(s"Now Listening to .. $audioStreamURI")

    case AudioStreamCreated(streamActor, request) =>
      log.info("Got AudioStreamActor, Send Subscription")
      streamActor ! SubscribeToAudioStream


    case AudioMasterRegistration if maybeAudioMaster.isEmpty =>
      context watch sender()
      log.info("Recieved an AudioMaster")
      context.become(frontEndLogic(Some(sender())))

    case StreamlinkRegistration if !streamLinkRefs.contains(sender()) =>
      context watch sender()
      streamLinkRefs = streamLinkRefs :+ sender()

      log.info(s"Recieved a new Member $sender")
      log.info(s"New size is  ${streamLinkRefs.size}")
    case Terminated(a) =>

      log.error("WARNING: NODE A IS GOINGAWAY")

      streamLinkRefs = streamLinkRefs.filterNot( _ == a)
      maybeAudioMaster match  {
        case Some(am) =>
          if (am == a)
            context.become(frontEndLogic(None))
        case _ =>
      }


    case answere: StreamlinkRecieved =>
      log.info(s"Answere was: ${answere.uri}")
      maybeAudioMaster.map { ref =>
        log.info("We have a AudioMaster!")
        ref ! CreateAudioStream(answere.uri)
      }


    case request: LinkRequested =>
      log.info("LinkRequest arrived")
      if (end > 0) {
        val index = start + r.nextInt(end)
        implicit val timeout: Timeout = Timeout(5 seconds)
        streamLinkRefs(index) ! request

      }

    case jobFail: JobFailed =>
      log.info(s"The Job haad errors: ${jobFail.reason}")

    case _: MemberEvent => // ignore
  }

  override def receive: Receive = frontEndLogic(None)

  /*
  def receive: PartialFunction[Any, Unit] = {

    case AudioSubscription( audioStreamURI ) =>
      log.info(s"Now Listening to .. $audioStreamURI")

    case AudioStreamCreated(streamActor, request) =>
      streamActor ! SubscribeToAudioStream


    case AudioMasterRegistration if audioMasterRef.isEmpty =>
      log.info("Recieved an AudioMaster")
      audioMasterRef = Some(sender())
    case StreamlinkRegistration if !streamLinkRefs.contains(sender()) =>
      context watch sender()
      streamLinkRefs = streamLinkRefs :+ sender()

      log.info(s"Recieved a new Member $sender")
      log.info(s"New size is  ${streamLinkRefs.size}")
    case Terminated(a) =>

      log.info("Terminated an A")
      streamLinkRefs = streamLinkRefs.filterNot( _ == a)
      audioMasterRef = audioMasterRef match {
        case None => None
        case Some(actorRef) =>

          log.warning("AUDIOMASTER ABOUT TO GO!!!!")
          if (a == actorRef) {
            log.info("AUDIOMASTER GONE")
            None
          } else
            Some(actorRef)
      }


    case answere: StreamlinkRecieved =>
      log.info(s"Answere was: ${answere.uri}")
      audioMasterRef.map { ref =>
        log.info("We have a AudioMaster!")
        ref ! CreateAudioStream(answere.uri)
      }


    case request: LinkRequested =>
      log.info("LinkRequest arrived")
      if (end > 0) {
        val index = start + r.nextInt(end)
        implicit val timeout: Timeout = Timeout(5 seconds)
        streamLinkRefs(index) ! request

      }
    case _: MemberEvent => // ignore
  }*/
}

object Frontend    {
  def main(args: Array[String]): Unit = {

    println("Started to Run Frontend Node")

    // Override the configuration of the port when specified as program argument
    val port = if (args.isEmpty) "0" else args(0)

    println(s"Got port $port")

    val config = ConfigFactory.parseString(s"akka.remote.netty.tcp.port=$port").
      withFallback(ConfigFactory.parseString("akka.cluster.roles = [Frontend]")).
      withFallback(ConfigFactory.load())

    val system = ActorSystem("HitchSystem", config)

    val frontend_Act: ActorRef = system.actorOf(Props[Frontend], name = "Frontend")
    ClusterClientReceptionist(system).registerService(frontend_Act)

    /*
    import system.dispatcher

    println("Start Scheduler")
    system.scheduler.schedule(3.seconds, 10.seconds) {
    frontend_Act ! LinkRequested("https://go.twitch.tv/iddqdow")
    }*/
  }
}
