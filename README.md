Welcome to Hitch
===================


This project is meant to convert Twitch[^twitch] Streams into Audio Streams. 

[^twitch]: Owned by Amazon at https://twitch.tv



Documents
-------------

This piece of software is meant to work in cluster mode by using **Akka**[^akka]

[^akka]:  https://akka.io/

> - Clustering out of the box
> - Message-driven
> - Easy to adopt 

Structure
-------------------

This part describes the project structure

resources

: ffmpeg

> Used to convert Video in Audio streams
: gui
> Contains a prototypical, only-for-internal-use gui 

: streamlink
> Translates channel links of YT and Twitch into their actual HLS Streams

: application.conf
> General Application Config 

: seed1.conf
> Additional Config for first seed node of cluster

: seed2.conf
> Additional Config for second seed node of cluster

scala/riseas/hitch
:   actors

    > AudioMaster
    >> Manages the AudioStreamers via caching by given Videostreamlink
    Returns a Reference to AudioStreamers

    > AudioStreamer
    >> Converts Videostream (HLS-Link!) to Audio Stream using **FFMPEG**

    > Frontend
    >> the entry point for interaction with backend, experimental

    > Streamlink
    >> Converts a given channel-url (of YT or Twitch) into HLS-Videolink

:   Messages
> Defines **all** messages sent between actors. 

:   libraries
> Holds some fancy additional code snippets that might be handy like Memo[^4]

:   GuiController
> Defines callbacks and stuff for the GUI 

:   HitchBoot
> Starts the different Nodes (Actors) in **one** jvm. Actors can be start on separate JVMs on the same machine - or with changed config (cluster ip!) even on remote machines

[^4]: Mesmerization of pure functions: Given a set of parameters, the function always returns the same. A memo caches the return value sothat the function does not need to be reevaluated for the same tuple of parameters twice  