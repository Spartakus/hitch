package riseas.hitch.actors

import akka.actor.{Actor, ActorLogging, ActorRef, ActorSystem, Props, RootActorPath, Terminated}
import akka.cluster.{Cluster, Member}
import akka.cluster.ClusterEvent.MemberUp
import com.typesafe.config.ConfigFactory
import riseas.hitch.{AudioMasterRegistration, AudioStreamCreated, CreateAudioStream}

class AudioMaster extends Actor with ActorLogging {

  val cluster = Cluster(context.system)

  override def preStart(): Unit = cluster.subscribe(self,  classOf[MemberUp])
  override def postStop(): Unit = cluster.unsubscribe(self)


  def manageAudioStreamers(streamesOpen: Map[String, ActorRef]): Receive = {
    case MemberUp(member) => register(member)

    case request: CreateAudioStream =>

      log.debug("Recieved CreateAudioStream")

      val maybeActor = streamesOpen.get(request.videoStream)


      log.info("Check if stream already has an actor")
      maybeActor match {
        case Some(streamActor) =>
          log.info(s"Audio Stream for ${request.videoStream} found")

          // Return already existing Actor
          sender() ! AudioStreamCreated(streamActor, request)

        case None =>
          log.info(s"No Audio Stream for ${request.videoStream} found")

          // Create new one
          val newActor = context.actorOf(Props[AudioStreamer])
          context watch newActor

          // Give AudioStreaming Actor current request
          newActor forward request

          // Add to "Cache"
          val newstreamesOpen = streamesOpen + (request.videoStream -> newActor)
          context.become( manageAudioStreamers(newstreamesOpen) )
      }
    case Terminated(a) =>
      val newstreamesOpen = streamesOpen.filterNot {
        case (url, ref) => ref == a
      }

      context.become( manageAudioStreamers(newstreamesOpen) )
    case _ =>
  }

  def receive: PartialFunction[Any, Unit] = manageAudioStreamers( Map[String,ActorRef]() )

  def register(member: Member): Unit = {
    if (member.hasRole("Frontend")) {
      context.actorSelection(RootActorPath(member.address) / "user" / "Frontend") !  AudioMasterRegistration
    }
  }

}

object AudioMaster {

  def main(args: Array[String]): Unit = {
    val port = if (args.isEmpty) "0" else args(0)
    val config = ConfigFactory.parseString(s"akka.remote.netty.tcp.port=$port").
      withFallback(ConfigFactory.parseString("akka.cluster.roles = [AudioMaster]")).
      withFallback(ConfigFactory.load())

    val system = ActorSystem("HitchSystem", config)
    val audiomaster = system.actorOf(Props[AudioMaster], name = "AudioMaster")
  }
}
//""