package riseas.hitch

import scala.collection.mutable
import scalafx.scene.control.{Button, ListView, TextArea, TextField}
import scalafx.event.ActionEvent
import scalafxml.core.macros.sfxml
import scalafx.Includes._
import scalafx.application.Platform

trait GUIState {
  def addConsoleLine(text: String)
  def addLogLine(text: String)
  def getCommand(): String
  def addButtonEvent( callback: String => Unit )
}

@sfxml
class GuiController( consoleOut: TextArea,
                     loggerOut: TextArea,
                     commandline: TextField,
                     sendButton: Button)
                     extends GUIState {


  override def addConsoleLine(text: String): Unit = {
    consoleOut.text.setValue( consoleOut.text.getValue + "\n"+ text )
  }
  override def addLogLine(text: String): Unit = {
    loggerOut.text.setValue( loggerOut.text.getValue + "\n"+ text )
  }

  override def getCommand(): String = commandline.text.value

  var callbacks = mutable.MutableList[String => Unit]()

  override def addButtonEvent(callback: String => Unit): Unit = {
    callbacks += callback
  }

  def onClose(event: ActionEvent) {
    Platform.exit()
  }

  sendButton.onAction = handle {
    callbacks.foreach { fct =>
      fct(getCommand())
    }
  }


}
